Example repository for the fgit workshop
----------------------------------------

This illustrates the structure of a git tree containing several revisions.

See LICENSE for terms and conditions.

-- Olivier Berger
